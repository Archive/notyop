/*
 * File: word.c
 *
 * This file is part of NoTypo, a typo correction system.
 *
 * Copyright 1998, Nat Friedman <nat@nat.org>
 *
 * Created September 18, 1998.
 *
 * This file contains the word-by-word non-grammar-aware typo correction
 * algorithm.
 *
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <glib.h>

#include "typo.h"
#include "permute.h"
#include "word.h"

static char * typo_skip_to_word(typo_t * typo, char * string);
static char * typo_get_word(typo_t * typo, char * string);
static char * typo_skip_word(typo_t * typo, char * curr_pointer);

int
typo_correct_word_by_word(typo_t * typo, char * string, char ** dst)
{
  char * curr_pointer;

  /* Allocate more than enough space for the corrected string. */
  *dst = g_malloc((2 * strlen(string)) + 1);

  /* Initialize this new string */
  strcpy(*dst, "");

  curr_pointer = string;
  while (*curr_pointer != '\0')
    {
      char * next_word_pointer;
      char * curr_word;

      /*
       * We correct the string on a word-by-word basis.  See the
       * documentation for notes about why this is not the best scheme.
       * Skip to the next applicable word in the string.
       */
      next_word_pointer = typo_skip_to_word(typo, curr_pointer);

      /*
       * Copy all of the skipped text (which may include numbers,
       * punctuation, etc) to the end of the result buffer, *dst.
       */
      if ((next_word_pointer - curr_pointer) > 0)
	{
	  int dst_len = strlen(*dst);
	  strncpy(*dst + dst_len, curr_pointer,
		  next_word_pointer - curr_pointer);
	  (*dst)[dst_len + (next_word_pointer - curr_pointer)] = '\0';
	}

      /*
       * This function, typo_get_word() allocates enough space to be
       * able to hold not only the current word, but the results of
       * any permutations on the current word.  We must be sure to
       * free this buffer when we are done with it.
       */
      curr_word = typo_get_word(typo, next_word_pointer);

      /*
       * There might not be another whole word that needs to be
       * corrected.  In that case, don't bother.
       */
      if (curr_word != NULL)
	{
	  /*
	   * This next function, will do all the work of correcting
	   * the word which it is passed.  The typo_correct_word_one()
	   * function runs all of the permutations one at a time,
	   * whereas typo_correct_word_all() runs all of the
	   * permutations in sequence, and therefore takes a lot
	   * longer to run.
	   */
	  if (typo->correct_multi_typos)
	    typo_correct_word_all(typo, curr_word);
	  else
	    typo_correct_word_one(typo, curr_word);

	  /*
	   * We now have a corrected word here which we must append to the
	   * result buffer.
	   */
	  strcat(*dst, curr_word);

	  /*
	   * Now update the pointers for the next go around, and free
	   * the memory which we allocated.
	   */
	  g_free(curr_word);
	}
      curr_pointer = typo_skip_word(typo, next_word_pointer);
    }

  return TRUE;
} /* typo_correct_word_by_word */  


/*
 * Correct typos in a single word by applying all permutations in
 * sequence; this is slower than typo_correct_word_one.  The provided
 * word must have no white space.  Returns TRUE on success and FALSE
 * on failure.
 */
int
typo_correct_word_all(typo_t * typo, char * word)
{
  char * permuted_word_insertion, * permuted_word_elision;
  char * permuted_word_inversion, * permuted_word_proxy;

  int max_index_elision, elision_index;

  unsigned long best_score;
  unsigned long orig_score;
  unsigned long min_score;

  char * best_word;

  orig_score = typo_word_score(typo, word);

  /*
   * If the provided word is a word in the lexicon, then perhaps we
   * don't want to try to find a better one after all.
   */
  if (orig_score > 0)
    if (typo->correct_valid_words == FALSE)
      return TRUE;

  /*
   * We allocate space for the permuted words once here.
   */
  permuted_word_inversion = (char *) alloca((2 * strlen(word)) + 1);
  permuted_word_insertion = (char *) alloca((2 * strlen(word)) + 1);
  permuted_word_elision = (char *) alloca((2 * strlen(word)) + 1);
  permuted_word_proxy = (char *) alloca((2 * strlen(word)) + 1);
  best_word = (char *) alloca((2 * strlen(word)) + 1);
  if ((permuted_word_inversion == NULL) || (best_word == NULL) ||
      (permuted_word_insertion == NULL) || (permuted_word_proxy == NULL) ||
      (permuted_word_proxy == NULL))
    {
      g_error("typo_correct_word_all: Out of memory\n");
      return FALSE;
    }

  strcpy(best_word, word);
  best_score = orig_score;

  /*
   *
   * Set min_score to be the minimum score that a word must have for it
   * to replace the original word.
   *
   */
  if (orig_score > 0)
    {
      min_score = orig_score + (orig_score * typo->valid_word_threshold);
    }
  else
    min_score = 0;

  /*
   * Run every permutation on the words in sequence.  The order of
   * nesting is: elision, insertion, proxy, inversion.  This is because
   * inversion is generally the most common typo, and so it is worth
   * checking first.  The algorithm will stop if it hits a match which
   * is match_stop_threshold better than the original word. FIXME
   *
   */
  max_index_elision = typo_permute_word_elision(typo, word, -1);
  for (elision_index = 0 ; elision_index <= max_index_elision;
       elision_index++)
    {
      int max_index_insertion, insertion_index;

      strcpy(permuted_word_elision, word);

      /*
       * The 'elision' permutation removes a single letter from the
       * candidate replacement
       */
      typo_permute_word_elision(typo, permuted_word_elision, elision_index);

      max_index_insertion =
	typo_permute_word_insertion(typo, permuted_word_elision, -1);
      for(insertion_index = 0 ; insertion_index <= max_index_insertion;
	  insertion_index ++)
	{
	  int max_index_proxy, proxy_index;

	  strcpy(permuted_word_insertion, permuted_word_elision);

	  /*
	   * The 'insertion' permutation inserts a single letter into
	   * the candidate replacement
	   */
	  typo_permute_word_insertion(typo, permuted_word_insertion,
				      insertion_index);

	  max_index_proxy =
	    typo_permute_word_proxy(typo, permuted_word_insertion, -1);
	  for (proxy_index = 0 ; proxy_index <= max_index_proxy;
	       proxy_index ++)
	    {
	      int max_index_inversion, inversion_index;

	      strcpy(permuted_word_proxy, permuted_word_insertion);

	      /*
	       * The 'proxy' permutation replaces a letter with a nearby
	       * letter on the user's keyboard.
	       */
	      typo_permute_word_proxy(typo, permuted_word_proxy, proxy_index);

	      max_index_inversion =
		typo_permute_word_inversion(typo, permuted_word_proxy, -1);
	      for (inversion_index = 0 ;
		   inversion_index <= max_index_inversion;
		   inversion_index ++)
		{
		  unsigned long curr_score;

		  strcpy(permuted_word_inversion, permuted_word_proxy);

		  /*
		   * The 'inversion' permtuation switches the order of
		   * two adjacent letters in the word
		   */
		  typo_permute_word_inversion(typo, permuted_word_inversion,
					      inversion_index);

		  curr_score = typo_word_score(typo, permuted_word_inversion);

		  if (curr_score > best_score)
		    {
		      best_score = curr_score;
		      strcpy(best_word, permuted_word_inversion);
		    }
		}
	    }
	}
    }

  /*
   * Don't replace the original word with the permutation unless the
   * permutation's score is above the minimum improvement.
   */
  if (best_score >= min_score)
    strcpy(word, best_word);

  return TRUE;
} /* typo_correct_word_all */

int
typo_correct_word_one(typo_t * typo, char * word)
{
  unsigned long orig_score, best_score, min_score, curr_score;
  char * best_word, * permuted_word;
  int max_index, i;

  orig_score = typo_word_score(typo, word);
  
  /*
   * If the provided word is a word in the lexicon, then perhaps we
   * don't want to try to find a better one after all.
   */
  if (orig_score > 0)
    if (typo->correct_valid_words == FALSE)
      return TRUE;

  permuted_word = (char *) alloca((2 * strlen(word)) + 1);
  best_word = (char *) alloca((2 * strlen(word)) + 1);
  if ((best_word == NULL) || (permuted_word == NULL))
    {
      g_error("typo_correct_word_one: Out of memory\n");
      return FALSE;
    }

  strcpy(best_word, word);
  best_score = orig_score;

  /*
   * Set min_score to be the minimum score that a word must have for it
   * to replace the original word.
   */
  if (orig_score > 0)
    {
      min_score = orig_score + (orig_score * typo->valid_word_threshold);
    }
  else
    min_score = 0;

  /*
   *
   * Run all of the permutations, one after the other.
   *
   */

  /* The 'elision' permutation removes one letter from the word. */
  max_index = typo_permute_word_elision(typo, word, -1);
  for (i = 0 ; i <= max_index ; i++)
    {
      strcpy(permuted_word, word);
      typo_permute_word_elision(typo, permuted_word, i);

      curr_score = typo_word_score(typo, permuted_word);
      if (curr_score > best_score)
	{
	  best_score = curr_score;
	  strcpy(best_word, permuted_word);
	}
    }

  /* The 'inversion' permutation switches the order of two letters in a word */
  max_index = typo_permute_word_inversion(typo, word, -1);
  for (i = 0 ; i <= max_index ; i++)
    {
      strcpy(permuted_word, word);
      typo_permute_word_inversion(typo, permuted_word, i);

      curr_score = typo_word_score(typo, permuted_word);
      if (curr_score > best_score)
	{
	  best_score = curr_score;
	  strcpy(best_word, permuted_word);
	}
    }

  /* The 'insertion' permutation adds one letter to a word */
  max_index = typo_permute_word_insertion(typo, word, -1);
  for (i = 0 ; i <= max_index ; i++)
    {
      strcpy(permuted_word, word);
      typo_permute_word_insertion(typo, permuted_word, i);

      curr_score = typo_word_score(typo, permuted_word);
      if (curr_score > best_score)
	{
	  best_score = curr_score;
	  strcpy(best_word, permuted_word);
	}
    }

  /*
   * The 'proxy' permutation replaces a letter with an adjacent
   * letter on the keyboard
   */
  max_index = typo_permute_word_proxy(typo, word, -1);
  for (i = 0 ; i <= max_index ; i++)
    {
      strcpy(permuted_word, word);
      typo_permute_word_proxy(typo, permuted_word, i);

      curr_score = typo_word_score(typo, permuted_word);
      if (curr_score > best_score)
	{
	  best_score = curr_score;
	  strcpy(best_word, permuted_word);
	}
    }

  /*
   * Don't replace the original word with the permutation unless the
   * permutation's score is above the minimum improvement.
   */
  if (best_score >= min_score)
    strcpy(word, best_word);

  return TRUE;
} /* typo_correct_word_one */

static char *
typo_skip_to_word(typo_t * typo, char * string)
{
  char * curr_pointer = string;

  g_assert(string != NULL);

  /*
   * We look for a substring which begins with an alphabetic character
   * which is either one character long or whose second character is
   * either "'" or another alphabetic character.  In our search, we
   * skip over whitespace, punctuation, and digits.
   */
  while (*curr_pointer != '\0')
    {
      /* In English, skip over "'s" */
      if (typo->language == TYPO_LANG_ENGLISH)
	if ((*curr_pointer == '\'') && (toupper(*(curr_pointer + 1)) == 'S'))
	  curr_pointer +=2;

      if (isalpha(*curr_pointer))
	{
	  /*
	   * This is probably a word since the first two characters
	   * are alphabetic.
	   */
	  if (isalpha( *(curr_pointer + 1) ))
	    return curr_pointer;

	  /* This is probably "I'm" or "I've" or something */
	  if (*(curr_pointer + 1) == '\'')
	    return curr_pointer;

	  /* This is a single-character word */
	  if ( isspace( *(curr_pointer + 1) ) || (*(curr_pointer + 1) == '\0'))
	    return curr_pointer;
	}

      while ( (*curr_pointer != '\0') && !isalpha(*curr_pointer))
	curr_pointer ++;
    }

  return curr_pointer;
} /* typo_skip_to_word */

/*
 * Grab the word at the beginning of 'string'.  Allocate enough space
 * to hold lots of permutations of this word.
 */
static char *
typo_get_word(typo_t * typo, char * string)
{
  char * word_end;
  char * word;

  g_assert(string != NULL);

  word_end = string;

  if (*word_end == '\0' || isspace(*word_end))
    return NULL;

  while ( (*word_end != '\0') && !isspace(*word_end) )
    {
      if (typo->language == TYPO_LANG_ENGLISH)
	{
	  /*
	   * In English, we special-case words like "I've" "I'm" "I'd"
	   * "They're" etc.  We make the whole construction count as
	   * one word.  The only time when "'" means a word break in
	   * English is when it is followed by "'s" so that we can do
	   * "The dog's ... " etc without special cases.
	   */
	  if ( ispunct(*word_end) )
	    {
	      if (*word_end == '\'')
		{
		  if (!isalpha(*(word_end + 1)) ||
		      toupper(*(word_end + 1)) == 'S')
		    break;
		}
	      else
		break;
	    }
	}
      else
	{
	  if ( ispunct(*word_end) )
	    break;
	}

      word_end ++;
    }

  word = (char *) g_malloc((2 * (word_end - string)) + 1);

  strncpy(word, string, word_end - string);
  word[word_end - string] = '\0';

  return word;
} /* typo_get_word */

/*
 * Skip over a word.  curr_pointer must point to the beginning of the
 * word.
 */
static char *
typo_skip_word(typo_t * typo, char * curr_pointer)
{
  char * word_end;

  g_assert(curr_pointer != NULL);

  word_end = curr_pointer;
  while (*word_end != '\0')
    {
      if (typo->language == TYPO_LANG_ENGLISH)
	{
	  /*
	   * In English, ' doesn't count as a word end unless the following
	   * character is 's'.
	   */
	  if (!isalnum(*word_end))
	    {
	      if (*word_end == '\'')
		{
		  if (!isalpha(*(word_end + 1)) ||
		      toupper(*(word_end + 1)) == 'S')
		    return word_end;
		}
	      else
		return word_end;
	    }
	}
      word_end ++ ;
    }

  return word_end;
} /* typo_skip_word */

