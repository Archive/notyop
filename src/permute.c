/*
 * This file is part of NoTypo, a typo correction system.
 *
 * Copyright 1998, Nat Friedman <nat@nat.org>
 *
 * Created September 17, 1998
 *
 * This file contains the typo permutations.  These are the functions
 * which permute words in the typical ways that people typo.  These
 * are used to try to find a real word which matches a typo, by
 * repeated permutation of the typo so that it matches dictionary
 * words.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "typo.h"
#include "permute.h"

/*
 *
 * Switch two letters in a word.
 *
 */
int
typo_permute_word_inversion(typo_t * typo, char * word, int n)
{
  char temp_char;

  g_assert(word != NULL);

  /*
   * When permutation functions are called with n == -1, that
   * means that they should return the maximum value of n which
   * should be passed to them.
   */
  if (n == -1)
    return strlen(word) - 1;

  /* When n is 0, preserve the word */
  if (n == 0)
    return TRUE;

  /* Swap the characters at indices n and n-1 */
  temp_char = word[n - 1];
  word[n - 1] = word[n];
  word[n] = temp_char;

  return TRUE;
} /* typo_permute_word_inversion */

/*
 *
 * Remove a letter from a word.
 *
 */
int
typo_permute_word_elision(typo_t * typo, char * word, int n)
{
  g_assert(word != NULL);

  if (n == -1)
    return strlen(word);

  if (n == 0)
    return TRUE;

  /* Remove the character at index n - 1 */
  word[n - 1] = '\0';
  strcat(word, word + n);

  return TRUE;
} /* typo_permute_word_elision */

/*
 *
 * Replace a letter with the letter on a nearby key on whatever
 * type of keyboard the user happens to have.
 *
 */
int
typo_permute_word_proxy(typo_t * typo, char * word, int n)
{
  int num_adjacent_keys = TYPO_ADJACENT_KEY_MAX;
  char replacement_char;

  g_assert(word != NULL);
  g_assert(typo->adjacent_key != NULL);

  if (n == -1)
    return num_adjacent_keys * strlen(word);

  if (n == 0)
    return TRUE;

  n --;

  /*
   * Replace the character at index (n / num_adjacent_keys) with an
   * adjacent character on the keyboard.
   */
  replacement_char = toupper(word[n / num_adjacent_keys]);
  replacement_char =
    typo->adjacent_key[ ((replacement_char - 34) * num_adjacent_keys) +
		        (n % num_adjacent_keys) ];

  /* Preserve the case of the current character */
  if (islower(word[n / num_adjacent_keys]))
    word[n / num_adjacent_keys] = tolower(replacement_char);
  else
    word[n / num_adjacent_keys] = toupper(replacement_char);

  return TRUE;
} /* typo_permute_word_proxy */

/*
 *
 * Insert a letter into a word.  Try to get the capitalization right.
 *
 */
int
typo_permute_word_insertion(typo_t * typo, char * word, int n)
{
  char * temp_word;

  g_assert(typo != NULL);
  g_assert(word != NULL);

  if (n == -1)
    return 26 * (strlen(word) + 1);

  if (n == 0)
    return TRUE;

  n --;

  /* Insert a new character in between characters n/26 and ((n/26) + 1) */
  temp_word = (char *) alloca(strlen(word) + 2);
  if (temp_word == NULL)
    g_error("typo_permute_word_insertion: Out of memory\n");

  strcpy(temp_word, word);
  temp_word[n / 26] = (n%26) + 'a';
  strcpy(temp_word + (n / 26) + 1, word + (n/26));

  /*
   * Make the capitalization of the inserted character match the
   * capitalization of the surrounded characters in the following
   * manner:
   *
   * ... AxB ... ==> x should be uppercase
   * ... axb ... ==> x should be lowercase
   * ... Axb ... ==> x should be lowercase
   *
   */
  if ((n / 26) > 0)
    {
      int preceding, following;

      preceding = (n / 26) - 1;
      following = (n / 26) + 1;

      if (isalpha(temp_word[preceding]) && isalpha(temp_word[following]))
	if (isupper(temp_word[preceding]) && isupper(temp_word[following]))
	  temp_word[n / 26] = toupper(temp_word[n / 26]);
    }

  /*
   * We require that the provided string have enough space for
   * insertions.
   */
  strcpy(word, temp_word);

  return TRUE;
} /* typo_permute_word_insertion */
