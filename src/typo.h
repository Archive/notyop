
#ifndef __TYPO_H__
#define __TYPO_H__

#include <glib.h>

#define TYPO_ADJACENT_KEY_MAX 6
#define TYPO_ADJACENT_KEY_SIZE 61

/*
 * The types of keyboard layouts
 */
enum {
  TYPO_KEYBOARD_QWERTY_US,
};

/*
 * Natural languages
 */
enum {
  TYPO_LANG_ENGLISH,
  TYPO_LANG_OTHER,
};

/*
 * The types of correction algorithms
 */
enum {
  TYPO_ALGORITHM_WORD_BY_WORD,
};



typedef struct typo_struct {

  /*
   * The algorithm used for typo correction.
   */
  int algorithm;

  /*
   * The language that the user is typing in.
   */
  int language;
  
  /* The lexicon stores a list of words accompanied by their
     statistical likelihood.

     The lexicon_max_value variable specifies the value of the highest
     element in the lexicon.  The lexicon is supposed to be normalized
     to this value. */
  GHashTable * lexicon;
  unsigned long lexicon_max_value;

  /* This is an array of adjacent keys on the keyboard.  It is kept
     here in the typo corrector object so that different arrays can be
     swapped in depending on the user's keyboard type. */
  char * adjacent_key;

  /* If correct_valid_words is set to TRUE, the corrector will look
     for higher-scoring permutations even for those words that are in
     the lexicon.

     The valid_word_threshold variable specifies the percentage
     improvement in score that the permutation must have over the
     original word in order for it to replace the original word. */
  int correct_valid_words:1;
  double valid_word_threshold;

  /* If this value is set, then the typo corrector will attempt to
     correct words which have more than one typo in them, e.g.
     "ahoesd" instead of "shoes".  This makes the typo corrector run
     -much- slower and so is off by default. */
  int correct_multi_typos:1;

} typo_t;

/*
 * Prototypes */
typo_t * typo_new(void);
void typo_destroy(typo_t * typo);

int typo_lexicon_load(typo_t * typo, char * filename);
int typo_lexicon_flush(typo_t * typo);

int typo_correct(typo_t * typo, char * string, char ** dst);
int typo_correct_word(typo_t * typo, char * word);

int typo_keyboard_set(typo_t * typo, int keyboard_type);

unsigned long typo_word_score(typo_t * typo, char * word);

#endif /* __TYPO_H__ */
