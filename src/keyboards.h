
#ifndef __KEYBOARDS_H__
#define __KEYBOARDS_H__

/*
 * The arrays in this file are used by the 'proxy' permutation to
 * guess which key the user meant to hit.
 *
 * For any given character adjacent_key[toupper(that_character) + 33]
 * should be a list of characters that the user might have meant to
 * hit.
 *
 * All these lists should be the same length and that length should
 * be the length in TYPO_ADJACENT_KEY_MAX.  If you need to pad a string
 * to make it the proper length pad it with spaces.
 *
 */

#include "typo.h"

char * typo_adjacent_key_qwerty_us = 
   "qw    " /* Keys adjacent to '!' */ 
   "'pl   " /* Keys adjacent to '"' */ 
   "we    " /* Keys adjacent to '#' */ 
   "er    " /* Keys adjacent to '$' */ 
   "rt    " /* Keys adjacent to '%' */ 
   "yu    " /* Keys adjacent to '&' */ 
   "pl    " /* Keys adjacent to "'" */ 
   "io    " /* Keys adjacent to '(' */ 
   "op    " /* Keys adjacent to ')' */ 
   "ui    " /* Keys adjacent to '*' */ 
   "      " /* Keys adjacent to '+' */ 
   "mkl   " /* Keys adjacent to '' */ 
   "l     " /* Keys adjacent to '.' */ 
   "      " /* Keys adjacent to '/' */ 
   "op    " /* Keys adjacent to '0' */ 
   "q     " /* Keys adjacent to '1' */ 
   "qw    " /* Keys adjacent to '2' */ 
   "we    " /* Keys adjacent to '3' */ 
   "er    " /* Keys adjacent to '4' */ 
   "rt    " /* Keys adjacent to '5' */ 
   "ty    " /* Keys adjacent to '6' */ 
   "yu    " /* Keys adjacent to '7' */ 
   "ui    " /* Keys adjacent to '8' */ 
   "io    " /* Keys adjacent to '9' */ 
   "pl    " /* Keys adjacent to ':' */ 
   "pl    " /* Keys adjacent to ';' */ 
   "mkl   " /* Keys adjacent to '<' */ 
   "      " /* Keys adjacent to '=' */ 
   "l     " /* Keys adjacent to '>' */ 
   "      " /* Keys adjacent to '?' */ 
   "qw    " /* Keys adjacent to '@' */ 
   "qwsz  " /* Keys adjacent to 'A' */ 
   "vghn  " /* Keys adjacent to 'B' */ 
   "xdfv  " /* Keys adjacent to 'C' */ 
   "serfcx" /* Keys adjacent to 'D' */ 
   "wsdfr " /* Keys adjacent to 'E' */ 
   "drtgvc" /* Keys adjacent to 'F' */ 
   "ftyhbv" /* Keys adjacent to 'G' */ 
   "gyujnb" /* Keys adjacent to 'H' */ 
   "ujklo " /* Keys adjacent to 'I' */ 
   "huikmn" /* Keys adjacent to 'J' */ 
   "mnjiol" /* Keys adjacent to 'K' */ 
   "pok   " /* Keys adjacent to 'L' */ 
   "njkl  " /* Keys adjacent to 'M' */ 
   "bhjm  " /* Keys adjacent to 'N' */ 
   "iklp  " /* Keys adjacent to 'O' */ 
   "ol    " /* Keys adjacent to 'P' */ 
   "asw   " /* Keys adjacent to 'Q' */ 
   "edfgt " /* Keys adjacent to 'R' */ 
   "wedxza" /* Keys adjacent to 'S' */ 
   "rfghy " /* Keys adjacent to 'T' */ 
   "yhjki " /* Keys adjacent to 'U' */ 
   "cdfgb " /* Keys adjacent to 'V' */ 
   "qasde " /* Keys adjacent to 'W' */ 
   "zasdc " /* Keys adjacent to 'X' */ 
   "tghju " /* Keys adjacent to 'Y' */ 
   "asx   " /* Keys adjacent to 'Z' */ 
   "p     " /* Keys adjacent to '[' */ 
   "      " /* Keys adjacent to "\" */ 
   "      " /* Keys adjacent to ']' */ 
   "ty    " /* Keys adjacent to '^' */ 
;

#endif /* __KEYBOARDS_H__ */
