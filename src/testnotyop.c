/*
 * A simple program to do aggressive real-time typo correction using
 * NoTyop.  By Nat Friedman <nat@nat.org>.
 *
 */

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <ctype.h>
#include "typo.h"

int
main(int argc, char ** argv)
{
  struct termios t;
  typo_t * typo;
  char line[1024];
  int line_pos;

  if (argc < 2)
    {
      fprintf(stderr, "Usage: %s <lexicon>\n", *argv);
      exit (1);
    }

  /* Turn off cooked mode on stdin */
  tcgetattr(0, &t);
  t.c_lflag &= ~(ICANON|ECHO);
  tcsetattr(0, TCSANOW, &t);

  /*
   * Initialize the typo corrector object.
   */
  typo = typo_new();
  typo->correct_valid_words = FALSE;

  /* Load the lexicon */
  fprintf(stderr, "Loading lexicon...");
  fflush(stderr);
  typo_lexicon_load(typo, argv[1]);
  fprintf(stderr, "done\n");

  strcpy(line, "> ");
  line_pos = 2;
  while(1)
    {
      char * new;
      int c;

      printf("\r\033[K%s", line);

      c = getchar();

      /* If we read a backspace, remove the last character */
      if (c == 8 || c == 127)
	{
	  line_pos --;
	  if (line_pos < 0)
	    line_pos = 0;
	  line[line_pos]='\0';
	}
      else if (isprint(c))
	{
	  line[line_pos] = c;
	  line_pos ++;
	  line[line_pos] = '\0';
	}

      /*
       * When the user hits <ENTER> or <SPACE>, run typo correction on
       * whatever he typed.
       */
      if (c == '\n' || c == ' ')
	{
	  typo_correct(typo, line, &new);
	  strcpy(line, new);
	  free(new);
	  line_pos = strlen(line);
	}

      /* If he hit <ENTER>, clear the line */
      if (c == '\n')
	{
	  printf("\n");
	  strcpy(line, "> ");
	  line_pos = 2;
	}
    }

  typo_destroy(typo);

  return 0;
} /* main */
