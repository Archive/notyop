/*
 * File: word.h
 *
 * This file is part of NoTypo, a typo correction system.
 *
 * Copyright 1998, Nat Friedman <nat@nat.org>
 *
 * Created September 18, 1998.
 *
 */

#ifndef __WORD_H__
#define __WORD_H__

#include "typo.h"

int typo_correct_word_by_word(typo_t * typo, char * string, char ** dst);
int typo_correct_word_all(typo_t * typo, char * word);
int typo_correct_word_one(typo_t * typo, char * word);

#endif /* __WORD_H__ */
