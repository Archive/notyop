#ifndef __PERMUTE_H__
#define __PERMUTE_H__

#include "typo.h"

int typo_permute_word_inversion(typo_t * typo, char * word, int n);
int typo_permute_word_elision(typo_t * typo, char * word, int n);
int typo_permute_word_proxy(typo_t * typo, char * word, int n);
int typo_permute_word_insertion(typo_t * typo, char * word, int n);

#endif /* __PERMUTE_H__ */

