/*
 * This file is part of NoTypo, a typo correction system.
 *
 * Copyright 1998, Nat Friedman <nat@nat.org>
 *
 * Created September 17, 1998
 *
 * Please see http://notyop.nat.org/ for more information.
 *
 */
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <glib.h>

#include "keyboards.h"
#include "word.h"
#include "typo.h"

/*
 * Prototypes for static functions.
 */
static void typo_lexicon_flush_func(gpointer key, gpointer value,
				    gpointer user_data);

/*
 * Correct any typos in the provided string, 'string'.  Allocate space
 * in *dst and put the corrected string there.
 */
int
typo_correct(typo_t * typo, char * string, char ** dst)
{
  switch (typo->algorithm)
    {
    case TYPO_ALGORITHM_WORD_BY_WORD:
      return typo_correct_word_by_word(typo, string, dst);
    default:
      g_warning("typo_correct: Unknown algorithm!\n");
      return FALSE;
    }

} /* typo_correct */


/*
 * Create a new typo object.
 */
typo_t *
typo_new(void)
{
  typo_t * typo;

  typo = (typo_t *) g_malloc(sizeof(typo_t));

  typo->algorithm = TYPO_ALGORITHM_WORD_BY_WORD;
  typo->language = TYPO_LANG_ENGLISH;

  typo->lexicon = NULL;
  typo->lexicon_max_value = 0;
  
  typo->correct_valid_words = TRUE;
  typo->valid_word_threshold = 20.0;

  typo->correct_multi_typos = FALSE;

  typo->adjacent_key = typo_adjacent_key_qwerty_us;

  return typo;
} /* typo_new */

/*
 * Destroy a typo object.
 */
void
typo_destroy(typo_t * typo)
{
  g_assert(typo != NULL);

  if (typo->lexicon != NULL)
    {
      typo_lexicon_flush(typo);
    }

  g_free(typo);
} /* typo_destroy */

/*
 * Load a lexicon datafile into a typo corrector object.
 */
int
typo_lexicon_load(typo_t * typo, char * filename)
{
  char line[1024];
  FILE * f;

  g_assert(typo != NULL);
  g_assert(filename != NULL);

  f = fopen(filename, "r");
  if (f == NULL)
    {
      g_warning("typo_lexicon_load: Cannot open file %s\n", filename);
      return FALSE;
    }

  if (typo->lexicon == NULL)
    typo->lexicon = g_hash_table_new(g_str_hash, g_str_equal);

  /*
   * Read in each line in the file.  The lines are in the format:
   * [optional whitespace]<word><whitespace><count><newline>
   */
  while (fgets(line, sizeof(line), f) != NULL)
    {
      char * word_begin, * word_end, * count_begin;
      unsigned long count;

      /* Skip over any white space at the beginning of the line */
      word_begin = line;
      while (isspace(*word_begin) && (*word_begin != '\0'))
	word_begin ++;

      /* Ignore lines that begins with '#' or lines that are all whitespace */
      if ((*word_begin == '#') || (*word_begin == '\0'))
	continue;

      /* Find the end of the word */
      word_end = word_begin;
      while (!isspace(*word_end) && (*word_end != '\0'))
	word_end ++;

      if (*word_end == '\0')
	{
	  g_warning("typo_lexicon_load: Malformatted lexicon file: %s\n",
		    filename);
	  fclose(f);
	  return FALSE;
	}

      /* Null-terminate the word */
      *word_end = '\0';

      /*
       * Skip over the whitespace at the end of the word and find the 'count'
       * field.
       */
      count_begin = word_end + 1;
      while (isspace(*count_begin) && *count_begin != '\0')
	count_begin++;

      if (*count_begin == '\0')
	{
	  g_warning("typo_lexicon_load: Malformatted lexicon file: %s\n",
		    filename);
	  fclose(f);
	  return FALSE;
	}

      count = strtol(count_begin, NULL, 10);

      /*
       * Add this word to the lexicon.  For machines which cannot
       * cast an unsigned long to a (void *) without loss of information,
       * we must allocate space for each unsigned long.  Too bad for
       * them.
       */
#ifndef ULONG_NEQ_POINTER
      g_hash_table_insert(typo->lexicon, g_strdup(word_begin),
			  (gpointer) count);
#else 
      {
	unsigned long * count_entry;
	count_entry = g_malloc(sizeof(unsigned long));
	*count_entry = count;

	g_hash_table_insert(typo->lexicon, g_strdup(word_begin),
			    (gpointer) count_entry);
      }
#endif /* ULONG_NEQ_POINTER */
      
    }

  fclose(f);
  return TRUE;
} /* typo_lexicon_load */

/*
 * Deallocate all data in the lexicon.
 */
int
typo_lexicon_flush(typo_t * typo)
{
  g_assert(typo != NULL);

  if (typo->lexicon == NULL)
    return TRUE;

  g_hash_table_foreach(typo->lexicon, typo_lexicon_flush_func, NULL);

  g_hash_table_destroy(typo->lexicon);

  typo->lexicon = NULL;
  
  return TRUE;
} /* typo_lexicon_flush */

static void
typo_lexicon_flush_func(gpointer key, gpointer value, gpointer user_data)
{
  g_free(key);

#ifdef ULONG_NEQ_POINTER
  g_free(value);
#endif
} /* typo_lexicon_flush_func */

/*
 * Set the user's keyboard layout in the typo corrector object.
 */
int
typo_keyboard_set(typo_t * typo, int keyboard_type)
{
  g_assert(typo != NULL);

  switch(keyboard_type)
    {
    case TYPO_KEYBOARD_QWERTY_US:
      typo->adjacent_key = typo_adjacent_key_qwerty_us;
      break;
    default:
      g_warning("typo_keyboard_set: Unrecognized keyboard type!\n");
      return FALSE;
    }

  return TRUE;
} /* typo_keyboard_set */

/*
 * Return a word's score in the lexicon
 */
unsigned long
typo_word_score(typo_t * typo, char * word)
{
  unsigned long score;
  char * tmp_word;
  int i; 

  g_assert(typo != NULL);
  g_assert(word != NULL);

  tmp_word = (char *) alloca(strlen(word) + 1);
  if (tmp_word == NULL)
    {
      g_error("typo_word_score: Out of memory\n");
      return 0;
    }
  
  strcpy(tmp_word, word);
  for (i = 0 ; i < strlen(tmp_word) ; i++)
    tmp_word[i] = tolower(tmp_word[i]);

#ifndef ULONG_NEQ_POINTER
  score = (unsigned long) g_hash_table_lookup(typo->lexicon, tmp_word);
#else
  score = *(unsigned long *) g_hash_table_lookup(typo->lexicon, tmp_word);
#endif /* ULONG_NEQ_POINTER */

  return score;
} /* typo_word_score */
